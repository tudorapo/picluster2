# How to build a tiny little hadoop cluster for fun.

The basic source for the configuration was the below series:

https://medium.com/@jasonicarter/how-to-hadoop-at-home-with-raspberry-pi-part-2-b8ccfbe6ba9a

Of course it's highly outdated. The cluster is working now, but I'm afraid that
it's memory configuration needs stricter limits.

## How to prepare

Or what is not properly ansibled yet.
  - setting up and distributing the hadoop_user's ssh keys to allow the startup
scripts to run.
  - automated startup of the services.
  - only restart munin-node if the config changed - some handler needed.
